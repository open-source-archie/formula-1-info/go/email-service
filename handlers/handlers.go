package handlers

import (
	"bytes"
	"fmt"
	"gitlab.com/open-source-archie/formula-1-info/go/email-service/config"
	"gitlab.com/open-source-archie/formula-1-info/go/email-service/models"
	"go.uber.org/zap"
	"gopkg.in/gomail.v2"
	"html/template"
)

type EmailSender interface {
	SendEmail(User *models.User)
}

type emailHandler struct {
	logger *zap.Logger
	config *config.EmailHandler
}

func NewEmailHandler(logger *zap.Logger, cfg *config.EmailHandler) *emailHandler {
	return &emailHandler{
		logger: logger,
		config: cfg,
	}
}

func (eh *emailHandler) SendEmail(User *models.User) {
	tpl, err := eh.parseTemplate(User)
	if err != nil {
		eh.logger.Error(fmt.Sprintf("failed to parse email templates: %s", err))
	}

	m := gomail.NewMessage()
	result := tpl.String()
	// Create email contents
	// Todo: Fill subject and body
	subject := "F1 Info Signup"

	// Set message headers
	m.SetAddressHeader("From", eh.config.SenderAddress, eh.config.EmailName)
	m.SetHeader("To", User.Email)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", result)
	m.Attach("templates/confirmation.html")

	// SMTP settings
	d := gomail.NewDialer(
		eh.config.SMTPServer,
		eh.config.SMTPHost,
		eh.config.SenderAddress,
		eh.config.SenderPassword,
	)

	// Send email
	if err := d.DialAndSend(m); err != nil {
		eh.logger.Error(fmt.Sprintf("failed to send email: %s", err))
		return
	}
	eh.logger.Info(fmt.Sprintf("Email sent to: %s", User.Email))
}

func (eh *emailHandler) parseTemplate(data interface{}) (*bytes.Buffer, error) {
	templ, err := template.ParseFiles("templates/confirmation.html")
	if err != nil {
		return nil, err
	}

	var tpl bytes.Buffer
	if err = templ.Execute(&tpl, data); err != nil {
		return nil, err
	}
	return &tpl, nil
}