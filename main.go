package main

import (
	"fmt"
	_ "github.com/lib/pq"
	"gitlab.com/open-source-archie/formula-1-info/go/email-service/config"
	email "gitlab.com/open-source-archie/formula-1-info/go/email-service/grpc"
	"gitlab.com/open-source-archie/formula-1-info/go/email-service/handlers"
	"gitlab.com/open-source-archie/formula-1-info/go/email-service/servers"
	"go.uber.org/zap"
)

func main() {
	if err := run(); err != nil {
		panic(err)
	}
}

func run() error {
	// Setup logging
	logger, err := zap.NewDevelopment()
	if err != nil {
		return err
	}
	defer func(logger *zap.Logger) {
		err := logger.Sync()
		if err != nil {
			panic(err)
		}
	}(logger)

	// Get configs
	cfg, err := config.GetConfig()
	if err != nil {
		logger.Panic(fmt.Sprintf("failed to retrive config file: %s", err.Error()))
		return err
	}

	// Initialise handlers
	emailHandler := handlers.NewEmailHandler(logger, &cfg.EmailHandler)

	// Initialise servers
	emailServer := email.NewEmailServer(logger, emailHandler)
	gRCPServer := servers.NewgRCPServer(&cfg.Server, logger, emailServer)

	// Run servers
	gRCPServer.Run()

	return nil
}
