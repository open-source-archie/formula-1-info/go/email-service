package email

import (
	"fmt"
	"gitlab.com/open-source-archie/formula-1-info/go/email-service/handlers"
	"gitlab.com/open-source-archie/formula-1-info/go/email-service/models"
	"go.uber.org/zap"
	"golang.org/x/net/context"
)

type Server struct {
	logger 	*zap.Logger
	emailHandler	handlers.EmailSender
}

func NewEmailServer(logger *zap.Logger, emailHandler handlers.EmailSender) *Server {
	return &Server{
		logger:       logger,
		emailHandler: emailHandler,
	}
}

func (s *Server) mustEmbedUnimplementedEmailSenderServer() {
	panic("implement me")
}

func (s *Server) SendConfirmationEmail(ctx context.Context, user *User) (*ConfirmationResponse, error) {
	s.logger.Info(fmt.Sprintf("Recieved User from client: %s", user.Email))

	UserDTO := &models.User{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
	}

	s.emailHandler.SendEmail(UserDTO)

	return &ConfirmationResponse{
		EmailSent: true,
	}, nil
}