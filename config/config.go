package config

import (
	"gopkg.in/yaml.v2"
	"os"
)

type Config struct {
	EmailHandler EmailHandler `yaml:"EMAIL_HANDLER"`
	Server       Server       `yaml:"SERVER"`
}

type EmailHandler struct {
	EmailName      string `yaml:"EMAIL_NAME"`
	SenderAddress  string `yaml:"SENDER_EMAIL_ADDRESS"`
	SenderPassword string `yaml:"SENDER_EMAIL_PASSWORD"`
	SMTPServer     string `yaml:"SMTP_SERVER"`
	SMTPHost       int    `yaml:"SMTP_HOST"`
}

type Server struct {
	Port int `yaml:"PORT"`
}

// GetConfig loads the variables from config.ini
func GetConfig() (*Config, error) {
	f, err := os.Open("./config.yml")
	if err != nil {
		// Todo: handle error
		return nil, err
	}
	defer f.Close()
	// Todo: handle error

	var cfg Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	if err != nil {
		// Todo: handle error
		return nil, err
	}

	return &cfg, nil
}
