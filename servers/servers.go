package servers

import (
	"fmt"
	"gitlab.com/open-source-archie/formula-1-info/go/email-service/config"
	email "gitlab.com/open-source-archie/formula-1-info/go/email-service/grpc"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"net"
)

type Server struct {
	*grpc.Server
	config *config.Server
	logger *zap.Logger
	emailServer *email.Server
}

func NewgRCPServer(config *config.Server, logger *zap.Logger, emailServer *email.Server) *Server {
	return &Server{
		Server: grpc.NewServer(),
		config: config,
		logger: logger,
		emailServer: emailServer,
	}
}

func (s *Server) Run() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", s.config.Port))
	if err != nil {
		s.logger.Error(fmt.Sprintf("failed to listen on %d: %s", s.config.Port, err.Error()))
	}
	s.logger.Info(fmt.Sprintf("running on port %d", s.config.Port))

	email.RegisterEmailSenderServer(s.Server, s.emailServer)

	if err := s.Server.Serve(lis); err != nil {
		s.logger.Error(fmt.Sprintf("failed to serve on %d: %s", s.config.Port, err.Error()))
	}
}
