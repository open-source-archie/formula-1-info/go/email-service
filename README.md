# formula-1-info/go/email-service
This service is part of the F1 Info collection of microservices. It sends users a confirmation email when they sign up to the scheduled email service.

## License
[MIT](https://choosealicense.com/licenses/mit/)
